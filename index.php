<?php
   include "class.php";
   include "db.php";
   include "query.php";
?>
<html>
  <head>
    <title>Object Oriented PHP </title>
  </head>
  <body>
    <p>
    <?php
        $text = 'hello world';
        echo "$text And the Universe";
        echo '<br>';
        $msg = new Message();
        echo '<br>';
        echo Message::$count;
        //echo $msg->text;
        $msg->show();
        $msg1= new Message("A new text");
        $msg1->show();
        echo '<br>';
        echo Message::$count;
        $msg2= new Message();
        $msg2->show();
        echo '<br>';
        echo Message::$count;
        $msg3 = new redMessage('A red Message');
        $msg3->show();
        echo '<br>';
        $msg4 = new coloredMessage('A colored Message');
        $msg4->color = 'green';
        $msg4->show();
        echo '<br>';
        $msg4->color = 'yellow';
        $msg4->show();
        echo '<br>';
        $msg4->color = 'red';
        $msg4->show();
        echo '<br>';
        $msg5 = new coloredMessage('A colored Message');
        $msg5->color = 'black';
        showObject($msg5);
        showObject($msg1);
        $db = new DB('localhost','intro','root','');
        $dbc =$db->connect();
        $query = new Query($dbc);
        $q = "SELECT *FROM users";
        $result = $query->query($q);
        echo '<br>';
        //echo $result->num_rows;
        if($result->num_rows>0){
            echo '<table>';
            echo '<tr><th>Name</th><th>Email</th></tr>';
            while ($row = $result->fetch_assoc()){
                echo '<tr>';
                echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
                echo '</tr>';
            }
            echo '</table>';
        } else {
          echo "Sorry no results";
        }
   ?>
   </p>
  </body>
</html>